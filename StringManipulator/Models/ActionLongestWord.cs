﻿using StringManipulator.Interfaces;

namespace StringManipulator.Models
{
    public class ActionLongestWord : IBaseUserAction
    {
        public StringOption ReturnOption()
        {
            return StringOption.LongestWord;
        }
    }
}