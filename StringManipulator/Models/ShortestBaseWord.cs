﻿using System.Collections.Generic;
using System.Linq;
using StringManipulator.Interfaces;

namespace StringManipulator.Models
{
    public class ShortestBaseWord : IBaseWord
    {
        public int WordLength { get; set; }
        public string Word { get; set; } 
        public StringOption Option => StringOption.ShortestWord;
         
        public void FindMatchingWord(List<string> words)
        {
            if (!words.Any())
                return; 

            var word = words.Aggregate((min, current) => min.Length < current.Length ? min : current);

            /*var word = words.OrderByDescending(s => s.Length)
                .Last();*/
                 
            Word = word;
            WordLength = word.Length; 
        }
         

    }
}
