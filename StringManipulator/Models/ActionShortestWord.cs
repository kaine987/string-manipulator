using StringManipulator.Interfaces;

namespace StringManipulator.Models
{
    public class ActionShortestWord : IBaseUserAction
    {
        public StringOption ReturnOption()
        {
            return StringOption.ShortestWord;
        }
    }
}