﻿using System.Collections.Generic;
using System.Linq;
using StringManipulator.Interfaces;

namespace StringManipulator.Models
{
    public class LongestBaseWord : IBaseWord
    {
        public string Word { get; set; }
        public int WordLength { get; set; } 

        public StringOption Option => StringOption.LongestWord;
            
       
          
        public void FindMatchingWord(List<string> words)
        {
            if (!words.Any())
                return;
            
            var longestWord = words.Aggregate("", 
                (max, current) => max.Length > current.Length ? max : current);
                  
            /*var longestWord = words.OrderBy(s => s.Length)
                            .Last();*/
            
            Word = longestWord;
            WordLength = longestWord.Length;
        }
 
    }
}