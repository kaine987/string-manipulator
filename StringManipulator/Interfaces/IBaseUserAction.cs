﻿namespace StringManipulator.Interfaces
{
    public interface IBaseUserAction
    {
        StringOption ReturnOption();
    }

}
