namespace StringManipulator.Interfaces
{
    public interface IUserActionContext
    {
        StringOption Define(string userChoiceInput);
    }
}