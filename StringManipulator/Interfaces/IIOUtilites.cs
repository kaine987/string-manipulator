﻿using System.Collections.Generic; 

namespace StringManipulator.Interfaces
{
    public interface IIoUtilites
    {
        StringOption DefineUserAction(string userAction);
        List<string> ReadUserInput(string messageUser);
        void WriteUserInputToConsole(List<string> words);
        void WriteResultToConsole(IBaseWord manipulator); 
    }
}
