﻿using System.Collections.Generic; 

namespace StringManipulator.Interfaces
{ 
    public interface IWordsFinderFactory
    {
        IEnumerable<IBaseWord> Manipulators { get; set; }

        IBaseWord Execute(StringOption stringOption); 
    }
}
