﻿using System.Collections.Generic;

namespace StringManipulator.Interfaces
{
    public interface IBaseWord
    { 
        string Word { get; set; }
        int WordLength { get; set; } 
        void FindMatchingWord(List<string> words); 
        StringOption Option { get; }
    }

    public enum StringOption
    {
        LongestWord,
        ShortestWord
    }

}