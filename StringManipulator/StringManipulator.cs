﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using StringManipulator.Interfaces;
using StringManipulator.IO;
using StringManipulator.Models;
namespace StringManipulator
{
    internal class StringManipulator
    {
        private static IIoUtilites IoUtilities { get; set; }
        private static IWordsFinderFactory WordsFinderFactory { get; set; }
        // ReSharper disable once InconsistentNaming 
        private static IWindsorContainer Container;

        private static void Main(string[] args)
        {  
            RegisterDependencies();
            ResolveDependencies();

            StringOption stringOption = IoUtilities
                    .DefineUserAction("Would you like to find the 'longest' or the 'shortest' word? " +
                                   "\nWrite '1' for the longest, '2' for the shortest. Then press return: ");
            var userInput = IoUtilities
                    .ReadUserInput("Write input text here and press return: ");
              
            IBaseWord searchedBaseWord = WordsFinderFactory
                    .Execute(stringOption);  
                
            searchedBaseWord.FindMatchingWord(userInput);

            IoUtilities.WriteResultToConsole(searchedBaseWord);
        }
        
        #region privates

        private static void RegisterDependencies()
        {
            Container = new WindsorContainer()
                .Install(FromAssembly.InThisApplication());

            Container.Register(
                Component.For<IUserActionContext>().DependsOn(Dependency.OnValue<IWindsorContainer>(Container))
                        .ImplementedBy<ActionContext>(),
                Component.For<IBaseUserAction>().ImplementedBy<ActionLongestWord>()
                        .Named("actionLongest"),
                Component.For<IBaseUserAction>().ImplementedBy<ActionShortestWord>()
                        .Named("actionShortest"),
                Component.For<IIoUtilites>().ImplementedBy<IoUtilities>(),
                Component.For<IWordsFinderFactory>().DependsOn(Dependency.OnValue<IWindsorContainer>(Container))
                        .ImplementedBy<WordsFinderFactory>(),
                Component.For<IBaseWord>().ImplementedBy<LongestBaseWord>().Named("longest"),
                Component.For<IBaseWord>().ImplementedBy<ShortestBaseWord>().Named("shortest"));
        }

        private static void ResolveDependencies( )
        { 
            IoUtilities = Container.Resolve<IIoUtilites>(); 
            WordsFinderFactory = Container.Resolve<IWordsFinderFactory>(); 
        }

           
        #endregion
    }
}
