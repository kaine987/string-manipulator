﻿using System.Collections.Generic;
using System.Configuration;
using Castle.Windsor;
using StringManipulator.Interfaces;

namespace StringManipulator
{
    public class ActionContext : IUserActionContext
    { 
        // ReSharper disable once InconsistentNaming
        private readonly IDictionary<string, IBaseUserAction> OperationStrategy = new Dictionary<string, IBaseUserAction>();
         
        public ActionContext(IWindsorContainer container)
        {
            DefineUserActionsFromConfiguration(container);
        }


        public StringOption Define(string userChoiceInput)
        {
            return OperationStrategy[userChoiceInput]
                            .ReturnOption();
        }


        #region Privates

        private void DefineUserActionsFromConfiguration(IWindsorContainer container)
        {
            for (var index = 0; index < ConfigurationManager.AppSettings.Count; index++)
            {
                string appSettingsValue = ConfigurationManager.AppSettings[index];
                string appSettingsKey = ConfigurationManager.AppSettings.Keys[index];

                OperationStrategy.Add(appSettingsValue, container.Resolve<IBaseUserAction>(appSettingsKey));
            }
        }

        #endregion
    }
}
