﻿using System.Collections.Generic;
using System.Linq;
using Castle.Windsor;
using StringManipulator.Interfaces;

namespace StringManipulator
{ 
    public class WordsFinderFactory : IWordsFinderFactory
    { 
        
        public IEnumerable<IBaseWord> Manipulators { get; set; }

        public WordsFinderFactory(IWindsorContainer container)
        {
            Manipulators = new List<IBaseWord>
            {
                container.Resolve<IBaseWord>("longest"),
                container.Resolve<IBaseWord>("shortest")
            }; 
        }
        public virtual IBaseWord Execute(StringOption stringOption)
        { 
            var manipulator = Manipulators
                .First(x => x.Option.Equals(stringOption));
            
            return manipulator;
        } 
    }

}
