﻿using System;
using System.Collections.Generic;
using System.Linq;
using StringManipulator.Interfaces;

namespace StringManipulator.IO
{
    public class IoUtilities : IIoUtilites
    {
        // ReSharper disable once InconsistentNaming
        private readonly IUserActionContext ActionContext;

        public IoUtilities(IUserActionContext actionContext)
        {
            ActionContext = actionContext;
        }
        
        public StringOption DefineUserAction(string messageUser)
        {
            var userAction = ReadUserInput(messageUser)[0];

            return ActionContext.Define(userAction);
        }

        public List<string> ReadUserInput(string messageUser)
        { 
            Console.Write(messageUser);

            var inputString = Console.ReadLine();

            var words = new List<string>();
            if (inputString != null)
                words = inputString.Split(' ').ToList();

            WriteUserInputToConsole(words);

            return words;
        }

        public void WriteUserInputToConsole(List<string> words)
        {
            var index = 1;
            words.ForEach(x =>
            {
                Console.Write($"Word_{index} length: {x.Length}\n");
                index += 1;
            });
        }

        public void WriteResultToConsole(IBaseWord manipulator)
        {
            Console.Write($"\nMatching word: {manipulator.Word} \nLength: {manipulator.WordLength}\n");
            Console.ReadLine();
        }

    }
}
